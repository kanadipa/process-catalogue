const express = require('express')
const server = express()
const next = require('next')
const dev = process.env.NODE_ENV !== 'production'
if (dev) { require('dotenv').config() }
const host = process.env.IP  || '0.0.0.0';
const port = process.env.PORT || 8080;
const nextApp = next({ dev })

const bodyParser = require('body-parser')
const handle = nextApp.getRequestHandler()
const mongoose = require('mongoose')
require ('./controllers/act-ctrl')

const cors = require('cors')
const logger = require("morgan");
const activitiesRouter = require('./routes/activities-router')

// Connect to MongoDB with Mongoose
mongoose
    .connect(process.env.MONGODB_CONNECTION_STRING, { useNewUrlParser: true, useFindAndModify: false, useUnifiedTopology: true} )
    .catch(err => console.log(err));
const db = mongoose.connection
db.on('error', console.error.bind(console, 'MongoDB connection error:'));
db.once('open', () => {
    console.log('Connected to mongodb')
});

nextApp.prepare().then(() => {

    server.use(bodyParser.json({limit: '50mb', extended: true}))
    server.use(bodyParser.urlencoded({limit: '50mb', extended: true}))
    server.use(cors())
    server.use(logger('dev'));

    server.use('/api', activitiesRouter)

    server.all('*', (req, res) => {
        return handle(req, res)
    })
    server.listen(port, host, err => {
        if (err) throw err
        console.log(`Server is running on port ${port} with ip ${host}`)
    })

}).catch((ex) => {
    console.error(ex.stack)
    process.exit(1)
})
