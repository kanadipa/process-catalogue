import React, { Fragment, useState } from "react";
import Link from "next/link";
import SearchBar from "../search-bar/SearchBar";

export default function NavBar({ ovs, split}) {
  const links = [
    {
      to: "split",
      header: "Activity Splits",
      links: [
        {
          to: "https://www.linkedin.com/in/karen-diazpaucar",
          header: "More Information",
        },
        {
          to: "activities",
          header: "Index",
        },
        {
          to: "target",
          header: "Target",
        },
        {
          to: "roles",
          header: "Roles & Responsabilities",
        },
        {
          to: "duties",
          header: "Duties",
        },
        {
          to: "guidelines",
          header: "Guiding Principles",
        },
      ],
    },
    {
      to: "ovs",
      header: "Operational Value Stream",
      links: [
       /* {
          to: "ovsactivities",
          header: "Process View",
        },*/
        {
          to: "ovsdefinition",
          header: "Definition",
        },
      ],
    },
    
  ];
 

  const [isNavCollapsed, setIsNavCollapsed] = useState(true);

  const handleNavCollapse = () => setIsNavCollapsed(!isNavCollapsed);
  return (
    <div id="navbar-section" className="navbar fixed-top navbar-expand-lg ">
      <a className="navbar-brand" href="/ ">
        <img src="/images/sie-logo.png" width="110" alt="siemens"></img>
      </a>
      <button
        className="navbar-toggler"
        type="button"
        data-toggle="collapse"
        data-target="#navbarNav"
        aria-controls="navbarNav"
        aria-expanded={!isNavCollapsed ? true : false}
        aria-label="Toggle navigation"
        onClick={handleNavCollapse}
      >
        <span className="navbar-toggler-icon"></span>
      </button>
      <div className= {`${isNavCollapsed ? 'collapse' : ''} navbar-collapse`} 
        id="navbarNav">
        <ul className="navbar-nav">
          {links.map((link, index) => (
            <div key={index} className="dropdown">
              <li className="nav-item" key={index}>
                <Link
                  activeClass="active"
                  style={{ cursor: "pointer" }}
                  href={`/${link.to}`}
                  spy={true}
                  smooth={true}
                  offset={-70}
                  duration={500}
                  
                >
                  <a className="nav-link" onClick={handleNavCollapse}
                  >{link.header}</a>
                </Link>
                <div 
                  className="nav-drop dropdown-content"
                  >
                  {link.links.map((items, i2) =>
                    items.to.includes("http") ? (
                      <Link key={i2} href={items.to}>
                        <a target="_blank" rel="noreferrer">
                          {items.header}
                        </a>
                      </Link>
                    ) : (
                      <Link key={i2} href={`/${link.to}/#${items.to}`}>
                        <a> {items.header}</a>
                      </Link>
                    )
                  )}
                </div>
              </li>
            </div>
          ))}
          <div className="dropdown ">
            <li className="nav-item">
             
            </li>
          </div>

          <div className="dropdown">
            <li className="nav-item">
              <Link scroll={false} href={`/#contact`}>
                <a className="nav-link">Contact</a>
              </Link>
            </li>
          </div>
        </ul>
      </div>
      <div
        className="navbar-collapse collapse justify-contend-end"
        id="navbarNav"
      >
        <ul className="navbar-nav ml-auto">
          <li className="nav-item">
            <SearchBar split={split} ovs={ovs} />
          </li>
        </ul>
      </div>
    </div>
  );
}
