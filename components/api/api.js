import api from './axios'


export const updateSplit = (id, payload) => api.put(`/split/${id}`, payload)
export const updateOVS = (id, payload) => api.put(`/ovs/${id}`, payload)
export const deleteSplit = id => api.delete(`/split/${id}`)
export const deleteOVS = id => api.delete(`/ovs/${id}`)
export const getSplit = () => api.get(`/all/split`)
export const getAllDef = () => api.get(`/all/def`)
export const getOVS = () => api.get(`/all/ovs`)
export const overwriteAllOVS = (id, payload) => api.post(`/ovs/${id}`, payload)
export const overwriteAllSplit = (sequence, payload) => api.post(`/split/${sequence}`, payload)
export const overwriteAllDef = (id, payload) => api.post(`/def/${id}`, payload)


//export const insertOVS = payload => api.post(`/ovs`, payload)
//export const getOVSById = id => api.get(`/ovs/${id}`)
//export const insertSplit = payload => api.post(`/split`, payload)
//export const getSplitByGroupProducts = group_of_products => api.get(`/split/${group_of_products}`)
const apis = {
    getSplit,
    getOVS,
    getAllDef,
    overwriteAllSplit,
    overwriteAllOVS,
    overwriteAllDef,
    updateOVS,
    updateSplit,
    deleteOVS,
    deleteSplit
}

export default apis
