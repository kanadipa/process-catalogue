import React, { useEffect, useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch } from "@fortawesome/free-solid-svg-icons";
import Link from "next/link";

const SearchBar = ({ split, ovs }) => {
  const [keyword, setKeyword] = useState("");
  const [links, setLinks] = useState([]);
  function handleEvent(e) {
    setKeyword(e.target.value);
  }
  useEffect(() => {
  
    const filteredSplit = 
    [...split
      .reduce((map, obj) => 
      map.set(obj.products, ({keywords: obj.keywords, products: obj.products})), 
      new Map()).values()]
          .filter(x => x.keywords.map(value=> value.toLowerCase()).includes(keyword.toLowerCase())
          || (x.products.toLowerCase().includes(keyword.toLowerCase()))
          )
          .map((x) => ({ name: x.products, link: `/split/${x.products}` }));
    
    const filteredOVS = 
      [...ovs.reduce((map, obj) => 
        map.set(obj.process, ({keywords: obj.keywords, process: obj.process})), 
        new Map()).values()]
          .filter(x => x.keywords.map(value=> value.toLowerCase()).includes(keyword.toLowerCase())
          || (x.process.toLowerCase().includes(keyword.toLowerCase()))
          )
          .map((x) => ({ name: x.process, link: `/ovs/${x.process}` }));
    
    setLinks([...filteredSplit, ...filteredOVS]);
  }, [keyword]);

  return (
    <div className="dropdown">
      <div className="myInput">
        <div style={{ padding: "0 .5rem" }}>
          <FontAwesomeIcon className="search" size="1x" icon={faSearch} />
        </div>
        <div>
          <input
            value={keyword}
            placeholder="Search Activities..."
            onChange={handleEvent}
          ></input>
        </div>
      </div>
      {links.length > 0 && (
        <div className="dropdown-content">
          {links.map((l, i) => (
            <Link key={i} href={l.link} passHref>
              <a>{l.name}</a>
            </Link>
          ))}
        </div>
      )}
    </div>
  );
};

export default SearchBar;
