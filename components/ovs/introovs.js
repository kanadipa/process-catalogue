import React from "react";
import {Container, Row} from "react-bootstrap";
import Link from "next/link"

import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

export default function IntroOVS({ovs}){
    
    const dataset = ovs.map((items) => items);
    const datasetovs = dataset.sort((a, b) => a.order - b.order);
    
    return(
      <div  
      className="w-100 " 
      style={{
        justifyContent: 'center',
        color: "white",
        display:'flex'
       }}> 
       <section className=' text-center mx-5 ' style={{justifyContent:'center'}}>
     { [...new Set(datasetovs.map(item => item).map(i=> i.process))].map((items, index)=>(
            <div 
                className=' px-5 py-4 mx-1 my-1 '   
                style={{backgroundColor:'rgb(1,23,42)', borderRadius:'10px'}}>

                            <Link key={items.Group} href={`/ovs/${items}`} passHref>
                            <a  
                            className='subtitle'
                            style={{color: "white"}} >
                            {items}
                            </a>
                        </Link> 
                        </div>))}
</section>
        </div>
    )}
