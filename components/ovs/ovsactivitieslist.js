import React from "react";
import {Container, Row} from "react-bootstrap";
import Link from 'next/link'
import { faChevronCircleRight } from "@fortawesome/free-solid-svg-icons";

import PopUp from "../UI-Components/popup"


export default function OVSActivitiesList({ovs}){
  const datasetovs = ovs.sort((a,b)=>(a.order-b.order))
  
    return (
      <div> 
          <Container >
          <Row id='row' >
          { [...new Set(datasetovs.map(item => item.process))].map((items, i)=>(
            <div  key={i} className='col-sm-5 col-md-5 col-lg-5 mx-2 my-2 '> 
              <div 
                  style={{
                    borderRadius:' 10px',
                    backgroundColor: 'rgba(0,0,40)',
                    color: 'white',
                    minHeight:'15rem',
                    display:'flex',
                    justifyContent: 'center',
                    alignItems: 'center'
                    }}>
                  <div className='px-4'>
                  
                  { datasetovs.length > 0 && 
                    <PopUp 
                      icon={faChevronCircleRight}
                      popupTitle={items}
                      popupDescription={datasetovs[0].task}
                    />
                  } 
                        <Link key={items}href={`/ovs/${items}`} passHref>
                        <a  
                        className='px-2 subtitle'
                        style={{color: "white"}} >
                          {items}
                        </a>
                        </Link>
                        </div>   
                    
                </div>
                  </div>))}
                    </Row>
                    </Container></div> 
                )}
