import React, {useState} from 'react';
import Carousel from 'react-bootstrap/Carousel'

const features=[
    {
        name: 'Color Coding',
        description: "You can identify and filter the responsibles of the different processes and activities by their colors.",
        image: '/images/colors2.png',
        class: 'carousel-item active'
    },
    {
        name: 'Correlation between Activities Split and Operational Value Stream ',
        description: "Now, you can recognize the relation between End-to-End Processes and the Global Activity Split. ",
        image: '/images/correlation.png',
        class: 'carousel-item'
    },
    {
        name: 'Export to Excel',
        description: 'If you want to explore further the relationships and contents, you are able to download to your computer.',
        image: '/images/export.png',
        class: 'carousel-item'
    }
]

export default function Features(){
  
        const [index, setIndex] = useState(0);
      
        const handleSelect = (selectedIndex, e) => {
          setIndex(selectedIndex);
        };
    return(
        <Carousel  activeIndex={index} onSelect={handleSelect}>
        
        {features.map((items, i)=>(
               <Carousel.Item key={i}>
                <div className='container d-block w-100"' style={{textAlign:'center',maxWidth:'50em', minHeight:'28em', flex:'left'}}>
                <div className=' my-2 mx-3'> 
                <div className='subtitle'>{items.name}</div>
                <div className='text2  mt-2 mb-5'>{items.description}</div>
                </div>
                <img className=' mx-3 mt-2 mb-5' height='300vh' src={items.image}></img>
                </div>
                </Carousel.Item>
                ))}

</Carousel>
        
    )
}



