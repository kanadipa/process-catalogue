import React from 'react'
import { Container, Row } from 'reactstrap'
import Link from 'next/link'

export default function Contact (){

   const  contacts =[
      
       {
        name: "Person 1",
        department: "Creativity",
        section: "Brain",
        picsrc: '/images/person-male.png',
        link: 'https://www.linkedin.com/in/karen-diazpaucar/'
    },
    {
        name: "Person 2",
        department: "Imagination",
        section: "Emotions",
        picsrc: '/images/person-male.png',
        link: 'https://www.linkedin.com/in/karen-diazpaucar/'

    },
    
   ]
    return(<div className='pb-5' style={{textAlign:'center'}}>

            <Container >
                <Row className='row'>
            {contacts.map((items, index )=>(
                <div className="text2 col-12 col-md-6 col-lg-5 text-center" key={index}>
                <div className=" boxing">
                <a 
                    href={items.link} 
                    target="_blank">
                  <img
                    className="responsive-img my-3"
                    id="member-picture"
                    src={items.picsrc}
                    alt="headshot"
                    style={{
                      cursor: 'pointer',
                      color: "inherit", 
                      fontWeight: 'bold'}}
                  />
                  </a>
                  <div> 
                    <div className='subtitle'>
                      <Link href={items.link} >
                        <a 
                          style={{color: "inherit", fontWeight: 'bold'}} 
                          target="_blank" rel="noreferrer">{items.name}
                          </a>
                        </Link>
                      </div>
                    <div>{items.department}</div>
                    <div>{items.section}</div>
                  </div>
                  </div>
                  </div>
            ))}
            </Row>
            </Container>
        </div>
    )
}