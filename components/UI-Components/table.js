import React from "react";
import {
  Paper,
  TableContainer,
  Table,
  TableHead,
  TableCell,
  TableRow,
  TableBody,
  CircularProgress,
  Tooltip,
  Toolbar,
  IconButton,
  Button, 
  Checkbox,
  Drawer,
  TablePagination,
  TableFooter,
  Typography
} from "@material-ui/core";

import FilterListIcon from '@material-ui/icons/FilterList';
import DeleteIcon from '@material-ui/icons/Delete';
import PropTypes from "prop-types";
import Filter from "./filter";
import { useState, useEffect, useRef} from "react"; 
import Editable from '../maintenance/edit'
import api from '../api/api'

export default function CustomTable(props) {
  let {
    data,
    csvData,
    title,
    loading,
    actionOption,
    handleAction,
    actionButtonTitle,
    pagination,
    count,
    handleLoadMore,
    ActionIcon,
    numSelected
  } = props;

  for (let i = 0; i < data.length; i++) {
    delete data[i].__v;
    const sortProperties = (obj) => {
      return Object.keys(obj)
        .sort()
        .reduce(
          (acc, key) => ({
            ...acc,
            [key]: obj[key],
          }),
          {}
        );
    };
    // data[i] = sortProperties(data[i]);
  }
  const columns = data.length > 0 ? Object.keys(data[0]) : [];
  const rows = data.map(row=> Object.keys(row).map(x=> row[x]))
  const [selectedFilter, setSelectedFilter] = useState({});
  const [loadAllPressed, setLoadAllPressed] = useState(false);
  const [editing, setEdit] = useState(false)
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [selected, setSelected] = useState([])
  const textareaRef = useRef();
  const [isDeleting, setDelete] = useState(false)
  const [childID, setID] = useState('')

  useEffect(() => {
    setSelectedFilter({});
  }, [title]);

  useEffect(()=>{
    async function deleteData(){ 
      try {
          if (isDeleting){
              // setActionFeedback({status: "error", text:'No data found.'})
              await selected.map(x=> api.deleteGAS(x, {timeout: 1000 * 40,} ))
              await selected.map(x=> api.deleteE2E(x, {timeout: 1000 * 40,} ))
              await selected.map(x=> api.deleteCR(x, {timeout: 1000 * 40,} ))
              // setActionFeedback({status: "info", text:`Uploaded ${data.length} of ${totalLines} line items`})
          }
      } catch (error) {
          // setActionFeedback({status: "error", text:'Uploading failed.'})
          console.log(error)
          alert(error)
      }
    }
    if (isDeleting) {
      alert("Data has been deleted")
      console.log('deleted');
     // setLoading(true);
      deleteData();
      setDelete(false);
      
  }
  })

  // handle filter
const handleFilter = (filterName, filter) => {
    // load all items
    if (pagination && !loadAllPressed) {
      handleLoadClick();
    }
    setSelectedFilter({ ...selectedFilter, [filterName]: filter });
  };

  // filter data
  Object.keys(selectedFilter).forEach((key) => {
    if (selectedFilter[key]) {
      data = data.filter((doc) => doc[key].toString() === selectedFilter[key]);
    }
  });


 const truncateString = (s, title) => {
    if (s !== null && s !== undefined) {
      if (s.length > 100 && !title) {
        return s.toString().substring(0, 100) + "...";
      } else {
        return s.toString();
      }
    } else {
      return null;
    }
  };

  const handleLoadClick = () => {
    setLoadAllPressed(true);
    //handleLoadMore();
  };


  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleSelectAllClick = (event) => {
    if (event.target.checked) {
      const newSelecteds = data.map((n) => n['_id']);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };

  const isSelected = (id) => selected.indexOf(id) !== -1;

  const handleClick = (event, id) => {
    const selectedIndex = selected.indexOf(id);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, id);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
    }

    setSelected(newSelected);
  };

  
  

  const handleDelete = ()=>{
  setDelete(true)
  }



  return (
    <div
      style={{
        width: "100%",
        display: "flex",
        flexDirection: "column",
        flexGrow: 1,
        ...props.style,
      }}
    >
      <div className='title'>
       
        
          {title}
      </div>
      <>
     
        <Toolbar>
        
        {selected.length > 0 ? (
        <Typography  color="inherit" variant="subtitle1" component="div">
          {selected.length} selected
        </Typography>
      ) : (
        null
      )}

      {selected.length > 0 ? (
        <Tooltip title="Delete">
          <IconButton aria-label="delete" onClick={handleDelete}>
            <DeleteIcon />
          </IconButton>
        </Tooltip>
      ) : (
        null
      )}
      <div
          style={{ fontSize: "0.9rem", marginTop: ".5rem", display: "flex" }}
        >
          <div style={{ marginRight: "1rem" }}>
            <b>Item count:</b> {data.length}
          </div>
          <div>
            {Object.values(selectedFilter).filter((f) => f).length > 0 && (
              <>
                <b>Selected filter: </b>
                {Object.keys(selectedFilter)
                  .filter((f) => selectedFilter[f])
                  .map(
                    (f) =>
                      `${f} [${truncateString(selectedFilter[f], false)}]; `
                  )}
              </>
            )}
          </div>
        </div>
    </Toolbar>
        <TableContainer
          style={{ display: "flex", flexGrow: 1, minHeight:0, width: "100%" }}
          component={Paper}
        >
          <Table stickyHeader >
            <TableHead>
           
              <TableRow>
                <>
                <TableCell>
                  <Checkbox
                   // indeterminate={numSelected > 0 && numSelected < rowCount}
                  checked={data.length > 0 && selected.length === data.length}
                   onChange={handleSelectAllClick}
                    inputProps={{ 'aria-label': 'select all desserts' }}
                  />
                  </TableCell>
            {/*<TableCell>Edit</TableCell>*/}
                {actionOption && (
                  <TableCell
                    style={{
                      backgroundColor: 'rgba(0,0,40)',
                      color: "white",
                      whiteSpace: "nowrap",
                      borderRight: "1px solid white",
                      width: '300px'

                    }}
                  >
                    
                  </TableCell>
                  
                )}
                {columns
                  .filter((c) => c !== "_id" &&  c !==  "order"  &&  c !== "createdAt")
                  .map((c, i) => (
                    <TableCell
                      style={{
                        backgroundColor: 'rgba(0,0,40)',
                        color: "white",
                        whiteSpace: "nowrap",
                        width:'max-content'
                      }}
                      key={i}
                      
                    >
                      {c}
                      <span style={{ marginLeft: ".5rem", textAlign:'right' }}>
                      <Filter
                          filters={[
                            ...new Set(
                              data.map((i) => truncateString(i[c], true))
                            ),
                          ]}
                          handleFilter={handleFilter}
                          filterName={c}
                          selectedFilter={selectedFilter}
                        />
                      </span>
                    </TableCell>
                  ))}
                  </>
              </TableRow>
            </TableHead>
            <TableBody>
              {data.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row, index) => {
                 const isItemSelected = isSelected(row['_id']);
                 const labelId = `enhanced-table-checkbox-${index}`;
                 return(
                <TableRow 
                  key={row['_id']} 
                  id={row['_id']}
                  onClick={(event) => handleClick(event, row['_id'])}
                  role="checkbox"
                  aria-checked={isItemSelected}
                  selected={isItemSelected}>

                  <TableCell padding="checkbox">
                    <Checkbox
                      checked={isItemSelected}
                      inputProps={{ 'aria-labelledby': labelId }}
                    />
                  </TableCell>
                 {/*<TableCell>
                    <Button 
                      id={row['_id']}
                      variant='contained' 
                      onClick={()=>setEdit(true)}
                 >Edit</Button></TableCell>*/}

                  {actionOption && (
                    <>
                    
                    <TableCell
                      style={{ backgroundColor: 'rgba(0,0,40)', textAlign: "center", minWidth:'300px'}}
                    >
                      <Tooltip title={actionButtonTitle}>
                        <IconButton onClick={() => handleAction(row)}>
                          <ActionIcon style={{ color: "#dc0000" }} />
                        </IconButton>
                      </Tooltip>
                    </TableCell>
                    </>)}
                  {Object.keys(row)
                    .filter((k) => k !== "_id" &&  k !== "order"  &&  k !== "createdAt")
                    .map((k, i) => (
                      <TableCell 
                        key={i} 
                        style={
                          typeof row[k]=== 'object' &&
                          row[k][0] !== null &&
                          row[k][0] !== undefined &&
                          row[k][0].length > 20 ? 
                            {minWidth:'450px'}
                            : typeof row[k] !== 'object'&&
                              row[k].length > 35? 
                              {minWidth:'300px'}
                          :
                          {minWidth:'max-content'}}  >
                        <Editable
                          id={[k,row.sequence || row['_id']]}
                          childRef={textareaRef}
                          type="textarea"
                          text={row[k]}
                          row_id={row['_id']}
                          column = {k}
                          editing={editing} 
                        />
                    
                      </TableCell>
                    ))}
                </TableRow>
              )})}
              {loading && (
                <TableRow>
                  <TableCell align="center" colSpan={columns.length}>
                    <CircularProgress />
                  </TableCell>
                </TableRow>
              )}
              {data.length < count && !loadAllPressed && (
                <TableRow>
                  <TableCell align="center" colSpan={columns.length}>
                    <Button
                      onClick={handleLoadClick}
                      size="small"
                      fullWidth
                      variant="contained"
                    >
                      Load all
                    </Button>
                  </TableCell>
                </TableRow>
              )}
            </TableBody>
            
          </Table>
           
              
        </TableContainer>
        <Table>
          <TableFooter>
          <TableRow>
        <TablePagination 
                rowsPerPageOptions={[10, 50, 100, { value: -1, label: 'All' }]} 
                count={data.length}
                rowsPerPage={rowsPerPage}
                page={page}
                SelectProps={{
                  inputProps: { 'aria-label': 'rows per page' },
                  native: true,
                }}
                onChangePage={handleChangePage}
                onChangeRowsPerPage={handleChangeRowsPerPage}/>
           </TableRow></TableFooter></Table>
      </>
    </div>
  );
}

CustomTable.propTypes = {
  data: PropTypes.array,
  csvData: PropTypes.array,
  title: PropTypes.string,
  loading: PropTypes.bool,
  actionOption: PropTypes.bool,
  handleAction: PropTypes.func,
  actionButtonTitle: PropTypes.string,
  pagination: PropTypes.bool,
  count: PropTypes.number,
  handleLoadMore: PropTypes.func,
  style: PropTypes.object,
};
