import React from "react";

export default function Intro(props) {
 
 return(
     <div className='hero' style={{backgroundColor: props.backgroundColor, backgroundImage: props.backgroundImage}}>
         <div className='hero-content'>
                <div className='hero-title' > <span className='font1'>{props.title}</span>
                {props.title2}</div>
                    {props.text.map((item, index)=>(
                        <div key={index} className='hero-text'>{item}</div> 
                    ))}
                <div style={{textAlign:'right'}}>{props.button}</div>       
                </div>
                
            </div>
        )
    }

   