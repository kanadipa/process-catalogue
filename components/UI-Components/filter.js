import React, { useState, Fragment } from "react";
import PropTypes from "prop-types";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import Badge from "@material-ui/core/Badge";
import FilterList from "@material-ui/icons/FilterList";
import { withStyles } from "@material-ui/core/styles";

const StyledBadge = withStyles({
  badge: {
    backgroundColor: "white",
  },
})(Badge);

export default function Filter(props) {
  const { selectedFilter, filterName } = props;
  const [anchorEl, setAnchorEl] = useState(null);
  const filterMenuOpen = Boolean(anchorEl);

  const handleFilterClick = (e) => {
    setAnchorEl(e.currentTarget);
  };

  const handleFilterClose = () => {
    setAnchorEl(null);
  };

  const handleFilter = (filterName, filter) => {
    props.handleFilter(filterName, filter);
    handleFilterClose();
  };

  return (
    <Fragment>
      <span
        className="ml-2"
        style={{ cursor: "pointer" }}
        onClick={handleFilterClick}
      >
        <StyledBadge
          badgeContent={selectedFilter[filterName] ? 1 : 0}
          variant="dot"
        >
          <FilterList fontSize="small" />
        </StyledBadge>
      </span>
      <Menu
        style={{ maxWidth: "500px" }}
        anchorEl={anchorEl}
        keepMounted
        open={filterMenuOpen}
        onClose={handleFilterClose}
        PaperProps={{
          style: {
            maxHeight: 48 * 4.5,
          },
        }}
      >
        <MenuItem
          style={{ fontSize: "0.8rem", minHeight: "0.8rem" }}
          key="all"
          selected={selectedFilter === "All"}
          onClick={() => handleFilter(filterName, "")}
        >
          All
        </MenuItem>
        {props.filters.sort().map((filter, index) => (
          <MenuItem
            style={{ fontSize: "0.8rem", minHeight: "0.8rem" }}
            key={index}
            selected={filter === selectedFilter}
            onClick={() => handleFilter(filterName, filter)}
          >
            {filter}
          </MenuItem>
        ))}
      </Menu>
    </Fragment>
  );
}

Filter.propTypes = {
  selectedFilter: PropTypes.object,
  handleFilter: PropTypes.func,
  filterName: PropTypes.string,
};
