import React from "react";


const ButtonCustom = React.forwardRef(({ onClick, href }, ref) => {
    return (
      <button href={href} onClick={onClick} ref={ref}  
      className='mt-5'
      block='true'>

      </button>
    )
  })
    
export default ButtonCustom

