import React from "react";
import {useState} from 'react';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {  faTimesCircle } from '@fortawesome/free-solid-svg-icons';
import Popup from "reactjs-popup"

export default function PopUp(props){
    const CustomButton = React.forwardRef(({ open, ...props }, ref) => (
      
       <FontAwesomeIcon size='1x' style={{cursor:'pointer', float:'left', marginRight:'8px'}} className="icon" icon={props.icon} ref={ref} {...props}/>
      
      ));
    return(
        <Popup modal
        trigger={open => <CustomButton icon={props.icon} open={open} />}
        position="right center"
        closeOnDocumentClick
      >
                    
                    {close => (  
                    <div style={{
                            background:'white', 
                            fontSize:'17px', 
                            color:'rgba(0,0,40)'}} 
                            className=' px-5 py-2'>
                    <FontAwesomeIcon
                                size='3x'
                            id='icon'
                            icon={faTimesCircle} 
                            onClick={close}/>
                                <div className='subtitle px-2'>{props.popupTitle}</div>
                                <div className='py-4 subtitle-sub' style={{textAlign:'left'}}>
                                        {props.popupDescription}
                                        </div></div>)}
                </Popup>
            )
                    }