import React from "react";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import Link  from 'next/link'
import { faInfoCircle, faTimesCircle, faCircle } from '@fortawesome/free-solid-svg-icons';
import Popup from "reactjs-popup"

export default function Cards(props){
      
      const MyButton = React.forwardRef(({ onClick, href }, ref) => {
            return (
              <button href={href} onClick={onClick} ref={ref}  
                  className='card-button mt-5'
                  block='true'
                  variant="outline-light">
               {props.relationDestinyName}
              </button>
            )
          })

          const Icon = React.forwardRef(({icon, ...props }, ref)=> {
            return (
                  <FontAwesomeIcon 
                  id="icon" 
                  icon={icon} ref={ref} {...props}>
                  </FontAwesomeIcon>
            )
          })
          const CR_Icon = React.forwardRef(({ ...props }, ref)=> {
            return (
                  <div 
                  style={{cursor:'pointer'}}
                  className="circle" 
                  ref={ref} {...props}>CR
                  </div>
            )
          })

          const Resp = (item, i)=>(
            item.farbe !== undefined ?
            <div 
                  key={i}
                  className='button-resp my-4'
                  style={{ borderColor:item.farbe, backgroundColor:item.farbe}}>
                        {item.responsible}</div>
                  : 
                  <div 
                        key={i}
                        className='button-resp my-4'
                        style={{ borderColor:'blue', backgroundColor:'blue'}}>
                              {item.responsible}</div>
          )
      
            return(
            <div id={props.sequence}
            className='cards col-2'
            style={{
            backgroundColor:  props.condition ? props.farbe: 'rgba(210,240,0.6)' }}>
                                    
                                    <Popup className='popup' trigger={
                                          <Icon icon={faInfoCircle}/>}
                                                modal>
                                                {close => (  
                                                <div style={{
                                                      background:'white', 
                                                      fontSize:'15px', 
                                                      color:'rgba(0, 0, 40, 1)'}} 
                                                      className=' popup px-5 py-5'>
                                                <FontAwesomeIcon 
                                                      id='icon'
                                                      icon={faTimesCircle} 
                                                      className='fa-3x' 
                                                      onClick={close}/>
                                                            <div className='subtitle'>{props.activity}</div>
                                                            
                                                                  <div className='py-4 subtitle-sub' style={{textAlign:'left'}}>
                                                                        <ul>
                                                                        {props.description.map((item,i)=>(
                                                                              props.description.length > 1 ?
                                                                        <li key={i}>
                                                                              {item}
                                                                              </li>
                                                                              : <div key={i}>
                                                                              {item}
                                                                              </div>))}</ul>
                                                                        </div>
                                                                  <div 
                                                                        className="buttongroup"
                                                                        style={{textAlign:'left'}} 
                                                                       > 
                                                                  {props.tool!== undefined ?props.tool.map((x,i)=>(
                                                                        <div className='tool' 
                                                                              style={{color:'rgba(0,0,40)',border: 'solid '}}>{x}
                                                                              </div>)) 
                                                                              :null}
                                                                              </div>        
                                                                  <div className='buttongroup '
                                                                        style={{textAlign:'left'}} 
                                                                   >
                                                                        {props.responsibles.map((item,i)=>(
                                                                              Resp(item, i)
                                                                              ))}
                                                                              </div>
                                                                        </div>)}
                                                </Popup>
                                          {props.cr !== undefined && props.cr.length !== 0?
                                          <Popup className='popup' style={{top:'8%'}} trigger={<CR_Icon/> }
                                                modal nested
                                          >
                                                
                                                {close => ( 

                                                <div style={{
                                                      textAlign: 'center',
                                                      background:'white', 
                                                      fontSize:'17px', 
                                                      color:'rgba(0, 0, 40, 1)'}} 
                                                      className=' popup px-5 py-2'>
                                                            <FontAwesomeIcon 
                                                      id='icon'
                                                      icon={faTimesCircle} 
                                                      className='fa-3x' 
                                                      onClick={close}/>
                                                      <div className='subtitle'>Control Requirements</div>
                                                      {props.cr.map((item,i)=>(
                                                            <div key={i}>
                                                            <div className='subtitle font-weight-lighter'>PCMB Ref: {item.pcmb_ref}</div>
                                                            <div className='py-2 subtitle-sub'>
                                                                  {item.control_requirement_wording}
                                                                  </div>
                                                      </div>))}
                                                <Link href='https://apex2.cit.siemens.de/pls/apex/f?p=103:32:6737222539038::NO' passHref>
                                                      <a targe='_blank'className='buttonshp'>For more information</a>
                                                </Link>
                                                                  
                                                </div>)}  
                                          
                                          
                                          </Popup> :null}
                                                   
                                                <div className='mb-5 pb-5 pt-5 px-1 text2'> {props.activity}</div>
                                                <div className='sequence my-4'>{props.sequence}</div>
                                                <div className='my-4'id='responsible2'>{props.responsible2}</div>
                                                 <div className="buttongroup my-4" style={{position:'absolute',bottom:'21%', left:'3%'}}> 
                                                      {props.tool!== undefined ?
                                                            props.tool.map((x,i)=>(
                                                                  <div key={i} 
                                                                        className='tool' 
                                                                        style={{color:'rgba(0,0,40)',border: 'solid '}}>{x}
                                                                        </div>
                                                                        )) 
                                                      :
                                                      null}</div>  
                                                {props.responsibles !== undefined ? 
                                                      <div className='buttongroup my-4 ' 
                                                            style={{position:'absolute',bottom:'10%', left:'3%'}}>
                                                            {props.responsibles.map((item,i)=>(
                                                                  Resp(item, i)
                                                                  ))}</div> 
                                                                  :null}
                                                      {props.relation.length !== 0 ? (
                                                            <Link 
                                                                  state= {props.state}
                                                                  href={`/${props.relationDestiny}/${props.relation}#${props.relation[0]}`} 
                                                                  style={{color:"white"}} 
                                                                  scroll= {false}
                                                                  className='my-5'
                                                                  passHref>
                                                            <MyButton />
                                                            </Link>
                                                      
                                                
                                          ):  null}  
                                          </div>)}

                                          /*                                    <FontAwesomeIcon
                                                      icon={faCircle}
                                                      id='circleIcon'
                                                      style=
                                                            {{color:props.condition2}}></FontAwesomeIcon>*/