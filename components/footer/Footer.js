import React from 'react'
export default function Footer(props){
    return(
        <div style={{flexShrink: 0, padding: "1rem 1.5rem", display: "flex", flexDirection: "column", backgroundColor: "#01172A"}}>
          
            <div className="links">
                <div className="link">© KANADIPA, 2021</div>
                
            </div>
        </div>
    ) 
}
