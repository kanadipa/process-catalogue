import React  from "react";
import {Container, Row } from "react-bootstrap";

export default function AccountabilityDuties() {
    const duties =[
        {
           title:' Maintain completeness',
           content: [
              'To orchestrate & ensure that all activities of CF R Global Activity Split (GAS) are covered by the (respective) CF R unit',
           ]
        },
        {
           title: 'Ensure quality, compliance, communication',
           content: [
              'To ensure high quality of accounting, reporting and controlling for the assigned ARE in line with external and internal regulations',
              'Escalation level for CF R shared service providers of the ARE',
              'First point of contact for central project roll-outs e.g. IFRS 16',
           ]
        },
        {
           title: 'Maintain cost efficiency and ICFR control application',
           content: [
              'To achieve & maintain efficient cost position whilst keeping quality',
              'Ensure ICFR efficient control application - provide confidence for management (CEO/CFO)',
           ]
        },
     ]
     
    
     return(
        <div>
           <Container>
           <table className='boxes w-100'>
              <tbody>
               {duties.map((duty, index)=>(
                  <tr key={index}>
                     <td className='subtitle box-row' style={{textAlign:'left'}}> {duty.title}
                        </td>
                     <td  className='box-row ' >
                           {duty.content.map((item, index)=>(
                              <div key={index}>
                                 <p>{item}</p>
                              </div>
                              ))}
                        </td>
                     </tr>
                  ))}
            </tbody>
         </table>
        </Container>
        <div className='text2 py-2 my-2'> Possible organizations (business partner): 
        <ul>
            <li>Regional: CF R Hubs</li>
            <li>Business Partner: CF R 7, SHS AC (incl. regional setup), SFS AC, RE FIN AR</li>
            <li>CF R HQ: CF R 2, CF R 6</li>
            <li>CD: CT BC, Next47</li>
        </ul></div>
        <div id='condition'>* Affiliated Companies are Siemens AG and all legally separate companies in which Siemens AG holds directly or indirectly (= via other Affiliated Companies) a majority stake (a majority of shares or a majority of voting rights) or that have conducted enterprise agreements (e.g. control or profit and loss agreements) with another Affiliated Company or that are directly or indirectly (= via other Affiliated
Companies) dependent on Siemens AG.(See section 2.2.2, Siemens Circular No. 206 “Corporate Governance Rules for Siemens’ Equity Investments”)
</div>
        </div>
        
     )
  }