import React, {useState, Fragment} from "react";

import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';



import Link from 'next/link'

export default function ActivitiesList({split}){
  

const dataset = split.map((items)=>(items))
const datasetsplit = dataset.sort((a,b)=>(a.order-b.order))

return (
  <div  
    className="w-100 " 
    style={{
      justifyContent: 'center',
      color: "white"
     }}> 
      <section className='section-box'>
       
        {[...new Set(datasetsplit.map(item => item.group_of_products))].map((item, i)=>(
          
          <div key={i} 
            className='menubox mx-1'
            >
          <div className='py-2'>
                 <div 
                 className='subtitle-sub  px-3' 
                 style={{
                   textAlign:'left', 
                   paddingBottom:'0.3rem', 
                   borderBottom: '1px solid white'}}>{item}</div>
                   <ul style={{listStyleType:'none'}}>
                  {
                    [...new Set(datasetsplit.filter(x=>x.group_of_products === item).map(item => item.products))].map((listitems, i2)=>(
                     
                     <li className='px-2 pb-3'>
                       <Link  href={`/split/${listitems}/`} passHref>
                        <a 
                           style={{color: "white"}}
                            >{listitems}</a></Link>
                     
                     </li>
                      
                       
                    ))}</ul>
                </div>
         
          </div>))
                      }
       
        </section>
      </div>
     )}
