import React,{useState} from "react";
export default function Guidelines() {
    const guidelines=[
        {         
            content: [
                "Language Matters. A lot.",
                "Think in systems",
                "Question everything, and ask many questions",
                "Understand & internalize the medium",
                "Communicate clearly, succinctly and often",
                "Don't hurt anyone",
                "Have empathy"
            ]
        }

    ]

    return(
        <div>
        <div className='text2 px-4 text-center'>
            <p>Here is an example on how to showcase a list of principles into working better; I took Design Principles as example.</p>
        </div>
         {guidelines.map((guideline, i)=>(
             <div key={i} className='px-4'>
                
                 <ol style={{padding:'0'}}>
                 <div className='row box-flex '>
                 {guideline.content.map((item, i2)=>(
                     <div
                        key={i2} 
                        className='col col-lg-10 col-sm-12 ' 
                        style={{
                            borderBottom:'1px solid'

                     }}><li className='py-3'>{item}</li> </div>
                   
               ))}
               </div></ol>
               </div>
         ))
      }

        </div>
    )
}