import React from "react";
import {Container } from "react-bootstrap";

export default function Roles() {
 const roles=[
    {
       title: 'Key User/ Process Expert',
       content: [
          'You can describe the responsabilitiy of actor 1 here.',
          'You can describe another responsabilitiy of actor 1 here.',
          'You can describe another responsabilitiy of actor 1 here.'
       ]
    },
    {
       title: 'Analyst',
       content: [
         'You can describe the responsabilitiy of actor 2 here.',
         'You can describe another responsabilitiy of actor 2 here.',
         'You can describe another responsabilitiy of actor 2 here.'

       ]
    },
    {
       title: 'Business User',
       content: [
         'You can describe the responsabilitiy of actor 3 here.',
         'You can describe another responsabilitiy of actor 3 here.',
         'You can describe another responsabilitiy of actor 3nhere.'
       ]
    },

   ]
   return(
      <div>
         <Container >
            <table className='boxes  w-100'>
               <tbody>
                  {roles.map((role, index)=>(
                     <tr key={index}>
                        <td className='subtitle box-row w-50' style={{textAlign:'left'}}> {role.title}
                           </td>
                        <td  className='box-row ' >
                           {role.content.map((item, index)=>(
                              <div key={index}>
                                 <p>{item}</p>
                              </div>
                           ))}
                        </td>
                        </tr>
                     ))}
                  </tbody>
               </table>
         </Container>
      </div>
   )
}