import React from "react";
import {Container, Row} from "react-bootstrap";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import { faArrowLeft, faArrowRight } from "@fortawesome/free-solid-svg-icons";

/*This is equivalent to page 4 in file CF R GAS 2020*/

export default function VisionDept1(){

    const values = [
        {
            subtitle: 'Creativity',
            text: 'It is the fuel for problem solving. And then you can keep describing why it makes the process thrive.'
        },
        {
            subtitle: 'Flexibility',
            text: 'It is the motor for communication. And then you can keep describing why it makes the stakeholder thrive.'
        },
        {
            subtitle: 'Accountability',
            text: 'Bringing ownership in your actions. And then you can keep describing why it makes you thrive.'
        },
        {
            subtitle: 'Ownership',
            text: 'Having the initiative to make impact. And then you can keep describing why it makes you also thrive.'
        }

    ]
    return(
        <div  >
                <div className='text2 w-100 py-4 text-center'>
                This part is about explaining the mission and vision of the Department 1 and the Activities List. For example:
                    
            </div>
            <div className='row'style={{display:'flex', alignItems:'center'}} >
                {values.map((x, index) => (
                <div className=' col-sm-5 col-md-4 col-lg-4 py-4 mx-2 my-2' 
                style={{border: 'solid white 2px', borderRadius: '25px'}}
                key={index}>
                        <div className='subtitle'>{x.subtitle}</div>
                        <div className='text2'>{x.text}</div>
                        </div>
                ))}
       </div>
    </div>
    )
}