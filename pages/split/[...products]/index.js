import React, { Fragment, useState } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import Cards from "../../../components/UI-Components/activitiescards";
import { Container, Row } from "reactstrap";

function GlobalFiltered({ split, cr }) {
  const router = useRouter();
  const { products } = router.query;
  const dataset = split.map((items, i1) => items);
  const datasetsplit = dataset.sort((a, b) => a.order - b.order);
  const [responsibility, setResponsibility] = useState("All");
  const colors = [
    {
      responsible: "All",
      farbe: "rgba(0,0,40,1)",
    },
    {
      responsible: "External",
      farbe: "rgba(153,153,169,1)",
    },
    {
      responsible: "Internal",
      farbe: "rgba(0,85,124,1)",
    },
    {
      responsible: "Audit",
      farbe: "rgba(0,100,110,1)",
    },
    {
      responsible: "Global",
      farbe: "rgba(0, 175, 142, 1)",
    }
  ];

  function WhatColor(resp) {
    //{colors.responsible.includes(activity1['Responsible'])

    let c1 = colors.filter((i1) => resp.indexOf(i1.responsible) >= 0);
    if (c1 !== undefined) return c1;
  }
  function CRMatching(seq) {
    let pcmb_ref = cr.filter((x) => x.cr_ref_split.indexOf(seq) >= 0);
    return pcmb_ref;
  }
  const condition2 = (data) =>
    ({ products }.products[0].split(",").indexOf(data.sequence) >= 0);

  //-------------------------------------------
  // Evaluate Sequence Call

  let filter1P = "";
  let resp_filtered = [];
  let filter2P = datasetsplit.filter(condition2).map((x) => x);
  if (filter2P.length === 0 && products) {
    if (filter1P === "") {
      filter1P = { products }.products[0];
    }
  }
  // Evaluate Product Call
  else
    datasetsplit.filter(condition2).map((items) => {
      if (filter1P === "") {
        filter1P = items.products;
      }
    });
  //-------------------------------------------
  // Evaluate Responsible Call

  let filter2R = undefined !== responsibility ? responsibility : "";
  filter2R = filter2R !== "All" ? filter2R : "";
  //-------------------------------------------

  return (
    <div
      className="pb-3"
      style={{  minHeight: "95vh",justifyContent:'center',display:'flex'}}
    >
      <div style={{maxWidth:'90vw'}}>
      {datasetsplit.filter(x => x.products.includes(filter1P)).map((items, i, array) =>{
        resp_filtered.push(items.responsibility)
        resp_filtered= resp_filtered.flat()
        resp_filtered = [...new Set(resp_filtered)]
        return(
          i===array.length-1?
          <div key={i}>
            <div
              style={{
                backgroundColor: "inherit",
                color: "inherit",
                paddingTop: "2%",
              }}
              className="page-title"
            >
              <span>
                <Link href="/global/#activities">
                  <a style={{ color: "inherit" }}>Activities Split</a>
                </Link>{" "}
                <span className="divider"> / </span>
              </span>
              <span>
                <a style={{ color: "inherit" }}>{items.group_of_products}</a>
                <span className="divider"> / </span>
              </span>
              <span className="active">{items.products}</span>
            </div>
            <div className='subtitle-sub'>{items.product_description}</div>
            <div className="buttongroup w-100">
              {colors.filter(x=> x.responsible=='All'  || resp_filtered.includes(x.responsible)).map((c, index) => (
                <button
                  key={index}
                  className="buttoncolors"
                  onClick={() => setResponsibility(c.responsible)}
                  style={{ backgroundColor: c.farbe, color: "white" }}
                >
                  {c.responsible}
                </button>
              ))}
            </div>{" "}
          </div>
        : null
       )} )}

      <Container fluid>
        <Row id="row">
          {datasetsplit.map((items, i1) => {
            return items.products === filter1P ? (
              filter2R === items.responsibility[0] || filter2R === "" ? (
                <Fragment key={i1}>
                  <Cards
                    //cr={CRMatching(items.sequence)}
                    activity={items.sipex_process_level_3}
                    relation={items.ovs_references}
                    relationDestiny="ovs"
                    relationDestinyName="Operational Value Stream"
                    description={items.description}
                    sequence={items.sequence}
                    farbe="#0db555"
                    condition={
                      { products } === undefined
                        ? false
                        : { products }.products[0]
                            .split(",")
                            .indexOf(items.sequence) >= 0
                    }
                    responsibles={WhatColor(items.responsibility[0])}
                  />
                </Fragment>
              ) : null
            ) : null;
          })}
        </Row>
      </Container>
      </div>
    </div>
  );
}

export default GlobalFiltered;
