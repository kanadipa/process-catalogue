import React from "react";
import Intro from "../../components/UI-Components/Intro";
import VisionDept1 from "../../components/split/vision";
import Roles from "../../components/split/roles";
import Guidelines from "../../components/split/guidelines";
import ActivitiesList from "../../components/split/activitieslist";

function Dept1({split}) {

const sections=[  {   
  id: 'activities',
  to: 'activities',
  titleContent: 'Index of Activities',
  component: <ActivitiesList split={split} />,
  styles: {
      titleColor: 'rgba(1,23,42)',
      backgroundColor: "white"}
  
},

    {
      id: "target",
      to: "target",
      titleContent: "Target of the Activities from Dept 1",
      component: <VisionDept1/>,
      styles: {
          titleColor: 'white',
          backgroundColor: "rgba(1,23,42)"}
    },
    {
      id: "roles",
      to: "roles",
      titleContent: "Roles & Responsabilities",
      
      component: <Roles />,
      styles: {
        titleColor: "white",
        backgroundColor: "#0117E3",
      },
    },
    {
      id: "guidelines",
      to: "guidelines",
      titleContent: "Guiding Principles",
      component: <Guidelines />,
      styles: {
        titleColor: 'rgb(1,23,42)',
        backgroundColor: "white"
      }
    },
  ];

  return (
    <div className="w-100">
      <Intro
        title="Activities Split Explorer"
        backgroundImage="linear-gradient(180deg, #01172A, #0117E3)"
        to="dept1"
        text={[
          "Multinational companies with international supply and value chains often operate from various locations in which traditional administrative activities take place.",
          "You can find the roles, responsibles, and guidelines on the performance of global activities that might interact with the operational value stream."
        ]}
      />

      <div id="home" className="w-100">
        {sections.map((section, index) => {
          return (
            <div
              key={index}
              id={section.id}
              style={{
                backgroundColor: section.styles.backgroundColor,
              }}
              className="w-100 d-flex flex-column align-items-center px-3 py-5"
            >
              <div
                className="w-100"
                style={{ maxWidth: "1050px", minHeight: "75vh" }}
              >
                <div>
                  <div
                    className="page-title text-center"
                    style={{ color: section.styles.titleColor }}
                  >
                    {section.titleContent}
                  </div>
                  <div
                    className="text-center"
                    className="subtitle"
                    style={{ color: section.styles.titleColor }}
                  >
                    {section.subtitle}
                  </div>
                </div>
                <div
                  className="w-100"
                  style={{ color: section.styles.titleColor }}
                >
                  {" "}
                  {section.component}
                </div>
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
}

export default Dept1;
