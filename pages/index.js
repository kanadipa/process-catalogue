import React from "react";
import Intro from "../components/UI-Components/Intro";
import Features from "../components/intro/features";
import Contact from "../components/intro/contact";
import Link from "next/link";

export default function Home() {
  const sections = [
    {
      id: "features",
      to: "features",
      titleContent: "Platform Features",
      component: <Features />,
      styles: {
        titleColor: "rgb(1,23,42)",
        backgroundColor: "white",
      }
    },
    {
      id: "contact",
      to: "contact",
      titleContent: "Contact Information",
      component: <Contact />,
      styles: {
        titleColor: "white",
        backgroundColor: "rgb(1,23,42)",
      }
    }
  ];
  return (
    <div className="w-100">
      <Intro
        title=" Process Catalogue "
        title2="& Related Information"
        backgroundColor="rgba(01,23,42,1)"
        text={[
          'This is an application for the showcasing a value stream and the activities from external divisions, where you can see the synergies between the processes in the stream. The information can be customizable.',
          'The application contains a database where the up-to date information from the processes is saved. '
              ]}
        button={
          <Link href='https://www.linkedin.com/in/karen-diazpaucar/' passHref>
            <a className=' navlink buttonshp mt-4 py-3 px-5 mx-2' target="_blank" rel="noreferrer">
              Learn more here
              </a>
            </Link>
 }
      />
      <div id="section" className="w-100">
        {sections.map((section, index) => (
          <div
            id={section.id}
            key={index}
            style={{
              backgroundColor: section.styles.backgroundColor,
            }}
            className="w-100 d-flex flex-column align-items-center px-3 py-5"
          >
            <div id="row" className="w-100" style={{ maxWidth: "1200px" }}>
              <div>
                <div
                  className="page-title"
                  style={{ color: section.styles.titleColor }}
                >
                  {section.titleContent}
                </div>
                <div
                  className="subtitle"
                  style={{ color: section.styles.titleColor }}
                >
                  {section.subtitle}
                </div>
              </div>
              <div className="row"></div>
              <div
                className="w-100"
                style={{
                  color: section.styles.titleColor,
                  backgroundColor: section.styles.backgroundColor,
                }}
              >
                {section.component}
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
}
