import React, { useEffect, useState } from "react";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap/dist/css/bootstrap.min.css";
import "react-app-polyfill/ie11";
import "react-app-polyfill/stable";
import "../public/App.css";
import Head from "next/head";
import Navbar from "../components/navbar/navbar";
import Footer from "../components/footer/Footer";
import api from "../components/api/api";

function App({ Component, pageProps }) {
  const [split, setGas] = useState([]);
  const [ovs, setOVS] = useState([]);

  const getData = async () => {
    const res1 = await api.getSplit({ timeout: 1000 * 30 });
    const res2 = await api.getOVS({ timeout: 1000 * 30 });
    setGas(res1.data.data);
    setOVS(res2.data.data);
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <div className="app">
     <Head>
        <title>Process Catalogue</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <Navbar split={split} ovs={ovs}  />
      <div style={{ marginTop: "75px" }}>
        <Component split={split} ovs={ovs} {...pageProps} />
        <Footer />
      </div>
    </div>
  );
}

export default App;
