import React from "react";
import Intro from "../../components/UI-Components/Intro";
import IntroOVS from "../../components/ovs/introovs";

export default function OVS({ ovs }) {
  const sections = [
    {
      id: "definition",
      to: "definition",
      titleContent: "Processes Definition",
      component: <IntroOVS ovs={ovs}/>,
      styles: {
        titleColor: "rgb(1,23,46)",
        backgroundColor: "white",
      },
    },
  ];
  return (
    <div className="w-100">
      <Intro
        title="Operational Value Stream"
        to1="ovs"
        text=
        {[
          "Operational value streams (OVS) are the sequence of activities needed to deliver a product or service to a customer. Examples include manufacturing a product, fulfilling an order, admitting and treating a medical patient, providing a loan, or delivering a professional service"]}
        backgroundColor="rgba(0,153,153)"
        backgroundImage="linear-gradient(180deg, #01172A, #c7238d)"
      />
      <div id="home" className="w-100">
        {sections.map((section, index) => (
          <div
            key={index}
            id={section.id}
            style={{
              backgroundColor: section.styles.backgroundColor,
            }}
            className="w-100 d-flex flex-column align-items-center px-3 py-5"
          >
            <div
              className="w-100"
              style={{ maxWidth: "1050px", minHeight: "65vh" }}
            >
              <div>
                <div
                  className="text-center page-title"
                  style={{ color: section.styles.titleColor }}
                >
                  {section.titleContent}
                </div>
                <div
                  className="text-center"
                  id="subtitle"
                  style={{ color: section.styles.titleColor }}
                >
                  {section.subtitle}
                </div>
              </div>

              <div
                className="w-100"
                style={{ color: section.styles.titleColor }}
              >
                {section.component}
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
}
