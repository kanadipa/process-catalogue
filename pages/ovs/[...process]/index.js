import React, { useState, Fragment } from "react";
import { useRouter } from "next/router";
import Link from "next/link";
import { Row, Container } from "reactstrap";
import Cards from "../../../components/UI-Components/activitiescards";
import { faArrowAltCircleRight } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

function OVSfiltered({ ovs }) {
  const router = useRouter();
  const { process } = router.query;
  const dataset = ovs.map((items) => items);
  const datasetovs = dataset.sort((a, b) => a.order - b.order);
  const [responsibility, setResponsibility] = useState("All");

  const colors = [
    {
      responsible: "All",
      farbe: "rgba(0,0,40,1)",
    },

    {
      responsible: "Business",
      farbe: "rgba(153,153,169,1)",
      // farbe: 'rgba(77,106,125,1)'
    },
    {
      responsible: "OVS Expert",
      farbe: "rgba(0,85,124,1)",
      //farbe: 'rgba(0,100,110,1)'
    },
    {
      responsible: "Technical",
      farbe: "rgba(137, 137, 255, 1)",
      //farbe: 'rgba(0,100,110,1)'
    },
    {
      responsible: "Analyst",
      farbe: "rgba(80,0,120,1)",
    },

    {
      responsible: "Key User",
      farbe: "rgba(170,50,190,1)",
    }
  ];
  function WhatColor(resp) {
    //{colors.responsible.includes(activity1['Responsible'])

    let c1 = colors.filter((i1) => resp.indexOf(i1.responsible) >= 0);
    if (c1 !== undefined) return c1;
  }

  const condition2 = (data) =>
    ({ process }.process[0].split(",").indexOf(data.sequence) >= 0);

  //-------------------------------------------
  // Evaluate Sequence Call
  let filter1P = "";
  let resp_filtered = [];
  let filter2P = datasetovs.filter(condition2).map((x) => x);
  
  if (filter2P.length === 0 && process) {
    if (filter1P === "") {
      filter1P = { process }.process[0];
    }
  }
  // Evaluate Product Call
  else
    datasetovs.filter(condition2).map((items) => {
      if (filter1P === "") {
        filter1P = items.process;
      }
    });

  //-------------------------------------------
  // Evaluate Responsible Call

  let filter2R = undefined !== responsibility ? responsibility : "";
  filter2R = filter2R !== "All" ? filter2R : "";
  //-------------------------------------------

  return (
    <div
      className="pb-3"
      style={{  minHeight: "95vh",justifyContent:'center',display:'flex' }}
      >
        <div style={{maxWidth:'90vw'}}> 

          {datasetovs.filter(x=>x.process.includes(filter1P)).map((items, i, array) =>{
            resp_filtered.push(items.responsibles)
            resp_filtered= resp_filtered.flat()
            resp_filtered = [...new Set(resp_filtered)]
            return(
              i===array.length-1?
              <div key={i}>
                <div
                  style={{ backgroundColor: "inherit", paddingTop: "2%", paddingRight:'0'}}
                  className="page-title"
                >
                  <span>
                    <Link href={`/ovs#ovsdefinition`}>
                      <a style={{ color: "inherit" }}>OVS</a>
                    </Link>{" "}
                    <span className="divider"> / </span>
                  </span>
                  <span>
                    <a>{items.process}</a>{" "}
                  </span>
                </div>
                <div className='subtitle-sub'>{items.subprocess_description}</div>
                <div className="buttongroup w-100">
                  {colors.filter(x=> x.responsible=='All'  || resp_filtered.includes(x.responsible) ).map((c, index) => {
                    return(

                    <button
                      key={index}
                      className="buttoncolors"
                      onClick={() => setResponsibility(c.responsible)}
                      style={{ backgroundColor: c.farbe, color: "white" }}
                    >
                      {c.responsible}
                    </button>
                   
                    )})}
                </div>{" "}
              </div>
             : null
          //  ) : null
          )})}
          <Container fluid className="px-3 w-100" style={{justifyContent:'left'}}>
            <Row>
              {datasetovs.map((activity, i) =>
                activity.process === filter1P ? (
                  activity.responsibles.includes(filter2R) || filter2R === "" ? (
                    <Fragment key={i}>
                      <Cards
                        activity={activity.process_step}
                        sequence={activity.sequence}
                        responsibles={WhatColor(activity.responsibles)}
                        tool={activity.tool}
                        relation={activity.split_reference}
                        relationOrigin="ovs"
                        relationDestiny="split"
                        relationDestinyName="Activity Split Explorer"
                        description={activity.task}
                        farbe="#0db555"
                        condition={
                          { process } === undefined
                            ? false
                            : { process }.process[0]
                                .split(",")
                                .indexOf(activity.sequence) >= 0
                        }
                      />
                      
                    </Fragment>
                  ) : null
                ) : null
              )}
            </Row>
          </Container>
    </div>
    </div>
  );
}

export default OVSfiltered;
