const mongoose = require('mongoose')
const Schema = mongoose.Schema

const SplitModel = new Schema(
    {   order: {type: Number, required: true},
        sequence: { type: String, required: true },
        product_number:{type: String, required: true},
        group_of_products: { type: String, required: true },
        keywords:{type:[String], required:false},
        products: { type: String, required: true },
        sipex_process_level_3: { type: String, required: true },
        product_description:{ type: String, required: true },
        description: { type: [String], required: true },
        responsibility: { type: [String], required: true },
        ovs_references: { type: [String], required: false }

    },
    { timestamps: true },
)

module.exports = mongoose.model('splitactivities', SplitModel, 'splitactivities')
