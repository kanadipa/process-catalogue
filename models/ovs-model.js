const mongoose = require('mongoose')
const Schema = mongoose.Schema

const OVSModel = new Schema(
    {
        order: {type: Number, required: true},
        sequence: { type: String, required: true },
        process_step: { type: String, required: true },
        split_reference: { type: [String], required: false },
        responsibles: { type: [String], required: false },
        department:{ type: String, required: true },
        process: { type: String, required: true },
        keywords:{type:[String], required: false},
        task: { type: [String], required: true },
        group_of_products: { type: String, required: true },
        tool: { type: [String], required: false },
        subprocess_description: { type: String, required: true },

    },
    { timestamps: true },
)

module.exports = mongoose.model('ovsactivities', OVSModel, 'ovsactivities')
