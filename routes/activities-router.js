const express = require('express')
const ActivitiesCtrl = require('../controllers/act-ctrl')

const router = express.Router()

//router.post('/split', ActivitiesCtrl.createSplit)

router.put('/split/:id', ActivitiesCtrl.updateSplit)
router.put('/ovs/:id', ActivitiesCtrl.updateOVS)

//router.get('/split/:group_of_products', ActivitiesCtrl.getSplitByGroupProducts)

router.get('/all/ovs', ActivitiesCtrl.getOVS)
router.get('/all/split', ActivitiesCtrl.getSplit)
router.get('/all/def', ActivitiesCtrl.getAllDef)
router.post('/split/:sequence', ActivitiesCtrl.overwriteAllSplit)
router.post('/ovs/:sequence', ActivitiesCtrl.overwriteAllOVS),
router.post('/def/:id', ActivitiesCtrl.overwriteAllDef)
//router.post('/ovs', ActivitiesCtrl.createOVS)
router.delete('/ovs/:id', ActivitiesCtrl.deleteOVS)
router.delete('/split/:id', ActivitiesCtrl.deleteSplit)



module.exports = router
