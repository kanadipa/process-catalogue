Welcome to The Process Catalogue

This is an app that is written on **ReactJS/Next JS** with a backend on **Express** that connects to the **MongoDB**. 

For starting, npm install. Then it is assumed you have MongoDB installed.

For Starting the db, do the following commands:

- cd ~/process_catalogue
- mkdir data
- mongod --dbpath=./data


On Compass, or a similar tool, create a DB called "process_catalogue" with the following collections:
- ovsactivities
- splitactivities

For _ovsactivities_, import the ovs-data.json file.
For _spliactivities_, import the split-data.json file. 

Once the DB is set up, initiate the app with _npm start_. Then, you can have

