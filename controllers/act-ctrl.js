const { any } = require('prop-types')
const GasModel = require('../models/split-model')
const OVSModel = require('../models/ovs-model')

/*
 * @param {Object} profile - Full user profile returned from MyID.
 */

getAllDef = async (req, res) => {
    await DefModel.find({}, (err, acts) => {
        if (err) {
            return res.status(400).json({ success: false, error: err })
        }
        if (!acts.length) {
            return res
                .status(404)
                .json({ success: false, error: `Abbreviation not found` })
        }
        return res.status(200).json({ success: true, data: acts })
    }).catch(err => console.log(err))
}   

getSplit = async (req, res) => {
    await GasModel.find({}, (err, acts) => {
        if (err) {
            return res.status(400).json({ success: false, error: err })
        }
        if (!acts.length) {
            return res
                .status(404)
                .json({ success: false, error: `Split not found` })
        }
        return res.status(200).json({ success: true, data: acts })
    }).catch(err => console.log(err))
}
getOVS = async (req, res) => {
    await OVSModel.find({}, (err, acts) => {
        if (err) {
            return res.status(400).json({ success: false, error: err })
        }
        if (!acts.length) {
            return res
                .status(404)
                .json({ success: false, error: `OVS not found` })
        }
        return res.status(200).json({ success: true, data: acts })
    }).catch(err => console.log(err))
}
overwriteAllSplit = async (req, res)=>{


    await GasModel.updateOne({ sequence: req.params.sequence},req.body,{upsert:true, new:true} )
    
        .then(() => {
        return res.status(200).json({
            success: true,
            sequence: sequence,
            message: 'Split updated!',
        })
    })
    .catch(error => {
        return res.status(200).json({
            error,
            message: 'Split upserted!',
        })
    })}

overwriteAllDef = async (req, res)=>{


    await DefModel.updateOne({ _id: req.params.id},req.body,{upsert:true, new:true} )
    
        .then(() => {
        return res.status(200).json({
            success: true,
            _id: id,
            message: 'Abbreviation updated!',
        })
    })
    .catch(error => {
        return res.status(200).json({
            error,
            message: 'Abbreviation upserted!',
        })
    })}
overwriteAllOVS = async (req, res)=>{

await OVSModel.updateOne({sequence: req.params.sequence},req.body, {upsert:true, new: true})
        .then(() => {
            return res.status(200).json({
                success: true,
                sequence: sequence,
                message: 'OVS updated!',
            })
        })
        .catch(error => {
            return res.status(200).json({
                error,
                message: 'OVS upserted!',
            })
        })
}
updateSplit = async (req, res) => {
    try {
     await GasModel.updateOne(
        { _id: req.params.id }, req.body, function (err, result) {
  
          if (err) {
            res.json({ result: "error", message: err.msg });
          }
          else {
            res.json({
              result: result,
              message: "Update Split Element data Successfully",
            });
          }
  
        }
      );
  
    } catch (err) {
      res.json({ result: "error", message: err.msg });
    }
  };
updateOVS = async (req, res) => {
try {
    await OVSModel.updateOne(
    { _id: req.params.id }, req.body, function (err, result) {

        if (err) {
        res.json({ result: "error", message: err.msg });
        }
        else {
        res.json({
            result: result,
            message: "Update OVS Element data Successfully",
        });
        }

    }
    );

} catch (err) {
    res.json({ result: "error", message: err.msg });
}
};

deleteSplit = async (req, res) => {
try {
    await GasModel.deleteOne(
    { _id: req.params.id }, req.body, function (err, result) {

        if (err) {
        res.json({ result: "error", message: err.msg });
        }
        else {
        res.json({
            result: result,
            message: "Deleted Split Element Successfully",
        });
        }

    }
    );

} catch (err) {
    res.json({ result: "error", message: err.msg });
}
};
deleteOVS = async (req, res) => {
    try {
        await OVSModel.deleteOne(
        { _id: req.params.id }, req.body, function (err, result) {
    
            if (err) {
            res.json({ result: "error", message: err.msg });
            }
            else {
            res.json({
                result: result,
                message: "Deleted OVS Element Successfully",
            });
            }
    
        }
        );
    
    } catch (err) {
        res.json({ result: "error", message: err.msg });
    }
    };

createOVS = (req, res) => {
    const body = req.body

    if (!body) {
        return res.status(400).json({
            success: false,
            error: 'You must provide a act',
        })
    }

    const act = new OVSModel(body)

    if (!act) {
        return res.status(400).json({ success: false, error: err })
    }

    act
        .save()
        .then(() => {
            return res.status(201).json({
                success: true,
                id: act._id,
                message: 'OVS created!',
            })
        })
        .catch(error => {
            return res.status(400).json({
                error,
                message: 'OVS not created!',
            })
        })
}
createSplit = (req, res) => {
const body = req.body

if (!body) {
return res.status(400).json({
    success: false,
    error: 'You must provide a act',
})
}

const act = new GasModel(body)

if (!act) {
return res.status(400).json({ success: false, error: err })
}

act
.save()
.then(() => {
    return res.status(201).json({
        success: true,
        id: act._id,
        message: 'Split created!',
    })
})
.catch(error => {
    return res.status(400).json({
        error,
        message: 'Split not created!',
    })
})
}

module.exports = {

    getSplit,
    getOVS,
   
    getAllDef,
    overwriteAllSplit,
    overwriteAllOVS,
    overwriteAllDef,
    updateOVS,
    updateSplit,
    deleteOVS,
    deleteSplit,
    //createSplit,
    //createOVS,
}